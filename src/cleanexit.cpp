/**
 * Simple signal handler for clean termination of an application.
 *
 * Author: Jan Schlüter <jan.schluter@lis-lab.fr>
 */

#include <atomic>
#include <csignal>
#ifdef __unix__
#include <unistd.h>
#endif

std::atomic<bool> _exit_requested(false);

void caught_signal(int)
{
    _exit_requested.store(true);
}

void allow_clean_exit() {
#ifdef __unix__
    struct sigaction sa = {0};
    sa.sa_handler = caught_signal;
    sigfillset(&sa.sa_mask);
    sigaction(SIGINT, &sa, NULL);
    sigaction(SIGTERM, &sa, NULL);
#else
    signal(SIGINT, caught_signal);
    signal(SIGTERM, caught_signal);
#endif
}

bool exit_requested() {
    return _exit_requested.load();
}
