/**
 * Simple signal handler for clean termination of an application.
 *
 * Author: Jan Schlüter <jan.schluter@lis-lab.fr>
 */

#ifndef CLEANEXIT_H
#define CLEANEXIT_H

/**
 * Set up signal handlers to make exit_requested() work.
 */
void allow_clean_exit();

/**
 * Check if the application was requested to terminate.
 * \returns whether the application was requested to terminate.
 */
bool exit_requested();

#endif
