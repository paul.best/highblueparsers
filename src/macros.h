#include <cstdint>  // For standard integer types like uint8_t, uint16_t, etc.

inline uint16_t BUILD_UINT16(uint8_t loByte, uint8_t hiByte) {
    return static_cast<uint16_t>((loByte & 0x00FF) + ((hiByte & 0x00FF) << 8));
}

inline int16_t BUILD_INT16(uint8_t hiByte, uint8_t loByte) {
    return static_cast<int16_t>((loByte & 0x00FF) + ((hiByte & 0x00FF) << 8));
}

inline uint32_t BUILD_UINT32(uint8_t Byte0, uint8_t Byte1, uint8_t Byte2, uint8_t Byte3) {
    return static_cast<uint32_t>(static_cast<uint32_t>(Byte0 & 0x00FF)
          + (static_cast<uint32_t>(Byte1 & 0x00FF) << 8)
          + (static_cast<uint32_t>(Byte2 & 0x00FF) << 16)
          + (static_cast<uint32_t>(Byte3 & 0x00FF) << 24));
}

inline uint64_t BUILD_UINT64(uint8_t Byte0, uint8_t Byte1, uint8_t Byte2, uint8_t Byte3,
                             uint8_t Byte4, uint8_t Byte5, uint8_t Byte6, uint8_t Byte7) {
    return static_cast<uint64_t>(static_cast<uint64_t>(Byte0 & 0x00FF)
          + (static_cast<uint64_t>(Byte1 & 0x00FF) << 8)
          + (static_cast<uint64_t>(Byte2 & 0x00FF) << 16)
          + (static_cast<uint64_t>(Byte3 & 0x00FF) << 24)
          + (static_cast<uint64_t>(Byte4 & 0x00FF) << 32)
          + (static_cast<uint64_t>(Byte5 & 0x00FF) << 40)
          + (static_cast<uint64_t>(Byte6 & 0x00FF) << 48)
          + (static_cast<uint64_t>(Byte7 & 0x00FF) << 56));
}