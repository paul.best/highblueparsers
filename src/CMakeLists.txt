add_executable(jasonrec
	recorder.cpp
	filewriter.cpp
	cleanexit.cpp
	main.cpp)
# compile as C++11
set_property(TARGET jasonrec
	PROPERTY CXX_STANDARD 11)
# do not export any symbols by default
set_property(TARGET jasonrec
	PROPERTY CXX_VISIBILITY_PRESET hidden)
set_property(TARGET jasonrec
	PROPERTY VISIBILITY_INLINES_HIDDEN true)

target_include_directories(jasonrec
	PRIVATE ${LIBUSB_INCLUDE_DIR})
target_link_libraries(jasonrec
	${LIBUSB_LIBRARY})

install(TARGETS jasonrec DESTINATION bin)
