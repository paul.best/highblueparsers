/**
 * SMIoT JASON Qualilife sound recording class.
 *
 * Author: Jan Schlüter <jan.schluter@lis-lab.fr>
 * Author: Maxence Ferrari <maxence.ferrari@lis-lab.fr>
 */

#ifndef RECORDER_H
#define RECORDER_H

#include <libusb.h>
#include <cstdint>
#include <vector>
#include <array>


#define MAX_MSG_LENGTH  65536
#define MAX_CHANNELS  6

/** Class for retrieving sample data from a JASON Qualilife sound card.
 */
class JasonRecorder {
    const std::int16_t VENDOR_ID  = 0x04D8;
    const std::int16_t PRODUCT_ID = 0x0053;
    const std::uint8_t ENDPOINT_SEND    = 0x01;
    const std::uint8_t ENDPOINT_RECEIVE = 0x81;

    // device control messages
    const std::uint8_t FRAME_START = 0xFE;
    const std::uint16_t START_ID     = 0x0C01;
    const std::uint16_t SET_SENSOR   = 0x0C09;
    const std::uint16_t SET_CLOCK_ID = 0x0C06;
    const std::uint16_t DATA_ID      = 0x0B01;
    const std::uint16_t STATUS_ID    = 0x0B02;

    const std::uint8_t START = 1;
    const std::uint8_t STOP = 0;
private:
    // libusb handles
    struct libusb_context *ctx;
    std::vector<struct libusb_device*> devices;
    struct libusb_device_handle *handle = NULL;
    /** Sends a message to the device, without payload.
     * \param[in] cmd The command identifier.
     */
    void send_message(std::uint16_t cmd);
    /** Sends a message with payload to the device.
     * \param[in] cmd The command identifier.
     * \param[in] payload The payload data to include.
     */
    void send_message(std::uint16_t cmd, std::vector<std::uint8_t> &payload);
    /** Sends a message with payload to the device.
     * \param[in] cmd The command identifier.
     * \param[in] payload Pointer to the payload data to include.
     * \param[in] length The size of the payload data in bytes.
     */
    void send_message(std::uint16_t cmd, std::uint8_t *payload, size_t length);
    /** Transform a float32 into an array of bytes.
     * \param[in] p A pointer to the array where the bytes of the float will be stored.
     * \param[in] index The starting position in the array `p` where the first byte will be stored.
     * \param[in] f The float value from which the bytes will be extracted.
     */
    void getBytesFromFloat(std::array<unsigned char, 4> &p, float f);

    // device messages sent back
    /** Reads a message from the device. Waits for a message if necessary.
     * \param[out] buffer Pointer to memory to write the message to.
     * \param[out] length Length of the buffer.
     * \param[in] max_wait Waiting time in milliseconds. Set to zero to wait
     * indefinitely. If no message could be read within this time, returns zero.
     * \returns the number of bytes received and written to the buffer.
     */
    size_t receive_message(uint8_t *buffer, size_t max_wait = 0);

    // device state, as far as known
    size_t additional_data_size = 730;
    std::uint8_t num_channels = 0;
    std::uint8_t depth = 0;
    std::uint8_t num_filter = 0;
    size_t sample_rate = 0;
    bool recording = false;
    bool verbose = false;
public:
    JasonRecorder(bool verbose);
    ~JasonRecorder();

    /** Searches for JASON Qualilife sound cards attached via USB.
     * \returns the number of devices found. May be zero.
     */
    size_t get_device_count();
    /** Selects the given device and opens it.
     * \param[in] number The device, must be smaller than get_device_count().
     */
    void set_device(size_t number);
    /** Starts recording from the device chosen with set_device(). Requires set_device() to be called before.
     * \param[in] num_channels The number of channels, between 1 and 5.
     * \param[in] sample_rate The number of samples per second (per channel).
     * \param[in] depth The number of bytes of each sample.
     * \param[in] num_filter The filter number (between 0 and 2).
     */
    void start_recording(int qhb_version, std::uint8_t num_channels, size_t sample_rate, std::uint8_t depth, std::uint8_t num_filter, size_t accelSamplingRate, size_t gyroSamplingRate, size_t magSamplingRate, size_t accelRangeScale, size_t gyroRangeScale, size_t magRangeScale);
    /** Stops recording from the device chosen with set_device(). */
    void stop_recording();
    /** Fetches a messages from the device chosen with set_device().
     * Requires the format to have been set before using set_format().
     * If the messsages contains samples, they will be put in samples.
     * \param[out] samples A vector the samples will be written to, replacing
     * existing content if any.
     * \param[in] planar If true, return the samples in planar form, i.e., one
     * channel after the other, the format sent by the device. If false (the
     * default), interleave the channels as done in PCM WAVE files.
     * \param[in] max_wait Maximum time in milliseconds to block waiting for a
     * packet from the device. If the time elapses before receiving any packets,
     * or if a different type of packet was received, returns with an empty
     * samples vector. Set to zero to wait indefinitely for a sample packet.
     */
    void get_samples(std::vector<std::uint8_t> &samples, std::vector<std::uint8_t> &imu_data, bool planar=false, size_t max_wait=0);
    /** Converts samples from planar to interleaved form.
     * \param[in] input A pointer to the planar samples to read.
     * \param[out] output A pointer to write the interleaved samples to.
     * \param[in] num_bytes The total number of bytes
     * to convert.
     * \param[in] num_channels The number of channels.
     * \param[in] depth The number of bytes per sample.
     */
    static void interleave_channels(std::uint8_t *input, std::uint8_t *output, size_t num_bytes,
                                    size_t num_channels, size_t depth);
};

#endif  // RECORDER_H
