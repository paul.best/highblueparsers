SMIoT JASON Qualilife sound recorder
====================================

This repository provides `jasonrec`, a command line application to record audio
samples from the JASON Qualilife sound card developed by SMIoT.

Prerequisites
-------------
To build and run the application, you will need:

* a C++ compiler supporting C++11 (e.g., g++ 4.8.1 and above)
* the `libusb` library and header
* CMake 3.1 or above (optional)

On a debian-based Linux system, these can be installed with:
```
sudo apt install g++ libusb-1.0-0-dev cmake
```

Compilation / Installation
--------------------------
Clone the repository somewhere or download and extract it.

### Using CMake
If cmake is available, create a build directory, compile, and install:
```
mkdir build
cd build
cmake .. -DCMAKE_BUILD_TYPE=Release
make
sudo make install/strip
```
This will install globally; pass `-DCMAKE_INSTALL_PREFIX=/some/directory` to
the `cmake` call to install to `/some/directory` instead. It is also possible
to run the compiled application from the `src` subdirectory of the `build`
directory, skipping installation altogether.

### Without CMake
If cmake is not available, you can still try to compile it manually. Just make
sure to link against `libusb`. For `g++`, an example `Makefile` is included in
the `src` directory, so the following may work:
```
cd src
make
```

### Under Windows
As for any other platform, there are multiple options on Windows. The following
has been tested successfully: Install CMake using the MSI installer from
https://cmake.org, install the Microsoft Visual Studio Build Tools from
https://aka.ms/buildtools (specifically, the C++ compiler), download and extract
the precompiled Windows binaries from https://libusb.info. Open the x64 Native
Tools Command Prompt and navigate to the source directory. Run the following:
```
mkdir build
mkdir install
cd build
cmake .. -DCMAKE_BUILD_TYPE=Release -DLIBUSB_INCLUDE_DIR=<libusb_dir>/include -DLIBUSB_LIBRARY=<libusb_dir>/MS64/dll/libusb-1.0.lib -DCMAKE_INSTALL_PREFIX=../install
nmake
nmake install
```
Replace `<libusb_dir>` with the path you extracted libusb to. If compilation and
installation succeeded, you will find a `jasonrec.exe` in `install/bin`. Copy
the `MS64/dll/libusb-1.0.dll` file from the libusb directory into `install/bin`.
You can now run `jasonrec.exe` from a command prompt, or by creating a shortcut
to it that includes suitable command line options.


Usage
-----
Running `jasonrec` without any arguments (or with any unsupported number of
arguments, in fact) will display information on its usage:
```
SMIoT JASON Qualilife sound recorder v1.3
Usage:jasonrec channels rate filename [--help, -h] [--chunk_len, -c CHUNK_LEN] [--total_len, -t TOTAL_LEN] [--device, -d DEVICE] [--bit_depth, -b BIT_DEPTH] [--imu, -i IMU] [--filter, -f FILTER] [--verbose, -v]
Positional arguments:
  CHANNELS:	number of channels to record (1 to 5)
  RATE:	sample rate in Hz to record at (integral number)
  FILENAME:	output file name. should include strftime() format specifiers
    if CHUNK_LEN is specified. For miliseconds, use %z. Example: location/recording_%Y%m%d_%H%M%S_%z.wav
Optional arguments:
-h, --help		show this help message and exit
  --bit_depth, -b	BIT_DEPTH:	Size of each samples in bits. Must be a multiple of 8. (Default: 16)
  --imu, -i	IMU:	IMU file name. Similar to FILENAME. Disable by default.
  --filter, -f	FILTER:	Number of the filter to use. Must be between 0 and 2. (Default: 0)
  --chunk_len, -c	CHUNK_LEN:	length per output file in seconds; will start a new file whenever
    this length is reached. If not given or zero, will record a single file.
  --total_len, -t	TOTAL_LEN:	Total recording length; will stop when this length is reached.
    If not given or zero, will record continuously until killed.
  --device, -d	DEVICE:	Which device to use in case multiple JASON cards are connected,
    where 0 is the first, 1 is the second card found (and so on).
  --verbose, -v		Enable the printing of status message 
```

As an example, to record a single 30-minute file of 2 channels at 16 kHz, run:
```
jasonrec 2 16000 recording.wav -t 1800
```

To record 4 channels at 128 kHz sample rate in 5-minute chunks with filenames
based on time stamps, without stopping, run:
```
jasonrec 4 128000 %Y-%m-%d_%H-%M-%S.wav -c 300
```
To record the same 4 channels at 128 kHz sample rate in 5-minute chunks with filenames
based on time stamps, without stopping, but with the saving of the imu data run:
```
jasonrec 4 128000 %Y-%m-%d_%H-%M-%S.wav -c 300 -i  %Y-%m-%d_%H-%M-%S.csv
```
File names may also include directory names based on time stamps, but the
directories have to be created in advance.
